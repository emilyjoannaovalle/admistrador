// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries


import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

//storage
import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAaeONhsE5P6blrVeCw8SMWTQrl9MKfuUA",
    authDomain: "administradorweb-595b7.firebaseapp.com",
    databaseURL: "https://administradorweb-595b7-default-rtdb.firebaseio.com",
    projectId: "administradorweb-595b7",
    storageBucket: "administradorweb-595b7.appspot.com",
    messagingSenderId: "369664447002",
    appId: "1:369664447002:web:b7640db8f0089b1ced77a3"
  };

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// Variables de la imagen
const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//declarar unas Variables global
var CodigoProducto = 0;
var tipoDeOro = "";
var Categoria = "";
var descripcion = "";
var urlImag = "";

//Funciones
function leerInputs() {
    const CodigoProducto = document.getElementById('txtCodigoProducto').value;
    const tipoDeOro = document.getElementById('txtTipoDeOro').value;
    const Categoria = document.getElementById('txtCategoria').value;
    const descripcion = document.getElementById('txtDescripcion').value;
    const urlImag = document.getElementById('txtUrl').value;

    return { CodigoProducto, tipoDeOro, Categoria, descripcion, urlImag };
}

function mostrarMensaje(mensaje) {
    const mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => { mensajeElement.style.display = 'none' }, 1000);
}

//Agregar productos a la DB
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
    alert("Ingrese a add db");
    const { CodigoProducto, tipoDeOro, Categoria, descripcion, urlImag } = leerInputs();
    //validar
    if (CodigoProducto === "" || tipoDeOro === "" || Categoria === "" || descripcion === "") {
        mostrarMensaje("Faltaron Datos por Capturar");
        return;
    }
    set(
        refS(db, 'Productos/' + CodigoProducto),
        {
            CodigoProducto: CodigoProducto,
            tipoDeOro: tipoDeOro,
            Categoria: Categoria,
            descripcion: descripcion,
            urlImag: urlImag
        }
    ).then(() => {
        alert("Se agrego con exito");
        Listarproductos();
    }).catch((error) => {
        alert("Ocurrio un error ")
    })
}

function limpiarInputs() {
    document.getElementById('txtCodigoProducto').value = '';
    document.getElementById('txtCategoria').value = '';
    document.getElementById('txtTipoDeOro').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs(CodigoProducto, tipoDeOro, Categoria, descripcion, urlImag) {
    document.getElementById('txtCodigoProducto').value = CodigoProducto;
    document.getElementById('txtCategoria').value = Categoria;
    document.getElementById('txtTipoDeOro').value = tipoDeOro;
    document.getElementById('txtDescripcion').value = descripcion;
    document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
    const CodigoProducto = document.getElementById('txtCodigoProducto').value.trim();
    if (CodigoProducto === "") {
        mostrarMensaje("No se ingreeso un Codigo de Producto");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Productos/' + CodigoProducto)).then((snapshot) => {
        if (snapshot.exists()) {
            const { tipoDeOro, Categoria, descripcion, urlImag } = snapshot.val();
            escribirInputs(CodigoProducto, tipoDeOro, Categoria, descripcion, urlImag);
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con codigo " + CodigoProducto + " No Existe.");
        }
    })
}

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarProducto);

//Listar Productos

function Listarproductos() {
    const dbref = refS(db, 'Productos');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbref, (snapshot) => {
        snapshot.forEach(childSnapshot => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.tipoDeOro;
            fila.appendChild(celdaNombre);

            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = data.Categoria;
            fila.appendChild(celdaPrecio);

            var celdaCantidad = document.createElement('td');
            celdaCantidad.textContent = data.descripcion;
            fila.appendChild(celdaCantidad);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImag;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true });
}


//Funcion actualizar

function actualizarProducto() {
    const { CodigoProducto, tipoDeOro, Categoria, descripcion, urlImag } = leerInputs();
    if (CodigoProducto === "" || tipoDeOro === "" || Categoria === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la informacion");
        return;
    }
    update(refS(db, 'Productos/' + CodigoProducto), {
        CodigoProducto: CodigoProducto,
        tipoDeOro: tipoDeOro,
        Categoria: Categoria,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizo con exito :)");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrio un error: " + error);
    });
}

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarProducto);

//Funcion Borrar

function eliminarProducto() {
    const CodigoProducto = document.getElementById('txtCodigoProducto').value.trim();
    if (CodigoProducto === "") {
        mostrarMensaje("No se ingreso un Codigo Valido.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Productos/' + CodigoProducto)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(refS(db, 'Productos/' + CodigoProducto)).then(() => {
                mostrarMensaje("Producto eliminado con éxito.");
                limpiarInputs();
                Listarproductos();
            }).catch((error) => {
                mostrarMensaje("Ocurrio un error al eliminar el producto: " + error);
            });
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con ID " + CodigoProducto + " no existe.");
        }
    });
}

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarProducto);

uploadButton.addEventListener('click', (event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if (file) {
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on('state_changed', (snapshot) => {
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
        }, (error) => {
            console.error(error);
        }, () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                txtUrlInput.value = downloadURL;
                setTimeout(() => {
                    progressDiv.textContent = '';

                }, 500);
            }).catch((error) => {
                console.error(error);
            });
        });

    }
});